# Next-generation NDS oscilloscope

`ndscope` is a tool for viewing time series data from the LIGO Network
Data Services (NDS).  Based on
[nds2-client](https://git.ligo.org/nds/nds2-client) and the
high-performance [pyqtgraph](http://pyqtgraph.org/) plotting library,
`ndscope` is a able to plot both online and offline data for many
channels simultaneously with intuitive mouse pan/zoom support.

![ndscope](ndscope.png)

### Features:

* fast online data with adjustable lookback window
* mouse pan and zoom, with background auto-fetch of new data
* auto-transition to second/minute trend data at appropriate zoom levels
* triggering, for true oscilloscope behavior
* cursors and crosshair in both time and Y axes
* save/load layout templates in YAML
* "StripTool mode", including direct reading of StripTool .stp files, and auto-backfill on startup
* NDS2 and NDS1 protocols supports
* python2/3 compatible

## Issues

Please report issues to the [gitlab issue tracker](https://git.ligo.org/cds/ndscope/issues).

## Requirements

Package requirements for `ndscope` (Debian package names):

* [python3-pyqtgraph](http://pyqtgraph.org/)
* [python3-nds2-client](https://git.ligo.org/nds/nds2-client)
* python3-pyqt5
* [python3-gpstime](https://git.ligo.org/cds/gpstime)
* python3-dateutil
* python3-yaml

The following packages are used for development purposes:

* pyqt5-dev-tools
* qt5-designer
* python3-setuptools_scm
* pytest3-pytest

`ndscope` is available for Debian 9 via the [LIGO CDSSoft Debian archive](https://git.ligo.org/cds-packaging/docs/wikis/home):

> deb http://apt.ligo-wa.caltech.edu/debian stretch main

You must also have the `NDSSERVER` environmental variable set, e.g. by adding
the following line to your `.bashrc` to access the CalTech NDS2 server:

```bash
export NDSSERVER=nds.ligo.caltech.edu
```

You can also use `nds.ligo-wa.caltech.edu` for the LHO NDS2 server or
`nds.ligo-la.caltech.edu` for the LLO NDS2 server.
